import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import {
  Card,
  DisplayText,
  Form,
  FormLayout,
  Layout,
  Page,
  PageActions,
  TextField,
  Banner,
  Frame,
  Toast,
} from '@shopify/polaris';
import store from 'store-js';

const UPDATE_PRICE = gql`
  mutation productVariantUpdate($input: ProductVariantInput!) {
    productVariantUpdate(input: $input) {
      product {
        title
      }
      productVariant {
        id
        price
      }
    }
  }
`;

const EditProducts = () => {
  const [data, setData] = useState({
    name: '',
    discount: '',
    price: '',
    variantId: '',
    showToast: true,
  });

  const [handleSubmit, { error, data: result }] = useMutation(UPDATE_PRICE);

  useEffect(() => {
    const item = store.get('item');
    const price = item.variants.edges[0].node.price;
    const variantId = item.variants.edges[0].node.id;
    const discounter = price * 0.1;
    const discount = (price - discounter).toFixed(2);
    setData({ ...data, price, variantId, discount });
  }, []);

  const handleChange = (value, name) => {
    setData({ ...data, [name]: value });
  };

  return (
    <Frame>
      <Page>
        <Layout>
          {error && <Banner status="critical">{error.message}</Banner>}
          {data.showToast && result && result.productVariantUpdate && (
            <Toast
              content="Sucessfully updated"
              onDismiss={() => setData({ ...data, showToast: false })}
            />
          )}
          <Layout.Section>
            <DisplayText size="large">{data.name}</DisplayText>
            <Form>
              <Card sectioned>
                <FormLayout>
                  <FormLayout.Group>
                    <TextField
                      prefix="$"
                      value={data.price}
                      disabled={true}
                      label="Original price"
                      type="price"
                    />
                    <TextField
                      prefix="$"
                      value={data.discount}
                      onChange={handleChange}
                      label="Discounted price"
                      type="discount"
                      id="discount"
                    />
                  </FormLayout.Group>
                  <p>This sale price will expire in two weeks</p>
                </FormLayout>
              </Card>
              <PageActions
                primaryAction={[
                  {
                    content: 'Save',
                    onAction: () => {
                      const productVariableInput = {
                        id: data.variantId,
                        price: data.discount,
                      };
                      setData({ ...data, showToast: true });
                      handleSubmit({
                        variables: { input: productVariableInput },
                      });
                    },
                  },
                ]}
                secondaryActions={[
                  {
                    content: 'Remove discount',
                  },
                ]}
              />
            </Form>
          </Layout.Section>
        </Layout>
      </Page>
    </Frame>
  );
};

EditProducts.propTypes = {};

export default EditProducts;
