import React, { useState } from 'react';
import {
  Button,
  Card,
  Form,
  FormLayout,
  Layout,
  Page,
  Stack,
  TextField,
  SettingToggle,
  TextStyle,
} from '@shopify/polaris';
import PropTypes from 'prop-types';

const AnnotatedLayout = () => {
  const [data, setData] = useState({
    discount: '10%',
    enabled: false,
  });

  const handleSubmit = () => {};

  const handleChange = (value, name) => {
    setData({ ...data, [name]: value });
    console.log(`${name} is : ${value}`);
  };

  const handleToggle = () => {
    setData({ ...data, enabled: !data.enabled });
  };

  const contentStatus = data.enabled ? 'Disable' : 'Enable';
  const textStatus = data.enabled ? 'enabled' : 'disabled';

  return (
    <Page>
      <Layout>
        <Layout.AnnotatedSection
          title="Default discount"
          description="Add a product to Sample App, it will automatically be discounted."
        >
          <Card sectioned>
            <Form onSubmit={handleSubmit}>
              <FormLayout>
                <TextField
                  id="discount"
                  value={data.discount}
                  onChange={handleChange}
                  label="Discount percentage"
                  type="discount"
                />
                <Stack distribution="trailing">
                  <Button primary submit>
                    Save
                  </Button>
                </Stack>
              </FormLayout>
            </Form>
          </Card>
        </Layout.AnnotatedSection>
        <Layout.AnnotatedSection
          title="Price updates"
          description="Temporarily disable all Sample App price updates"
        >
          <SettingToggle
            action={{
              content: contentStatus,
              onAction: handleToggle,
            }}
            enabled={data.enabled}
          >
            This setting is{' '}
            <TextStyle variation="strong">{textStatus}</TextStyle>.
          </SettingToggle>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
};

AnnotatedLayout.propTypes = {};

export default AnnotatedLayout;
