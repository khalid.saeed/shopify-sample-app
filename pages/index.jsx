import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { EmptyState, Layout, Page } from '@shopify/polaris';
import { ResourcePicker, TitleBar } from '@shopify/app-bridge-react';
import { set } from 'js-cookie';
import store from 'store-js';

import ResourceListWithProducts from '../components/ResourceList';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

const Index = () => {
  const [open, setOpen] = useState(false);
  const emptyState = !store.get('ids');

  const handleSelection = resources => {
    const { selection = [] } = resources;
    const idsFromResources = selection.map(product => product.id);
    store.set('ids', idsFromResources);
    setOpen(false);
  };

  return (
    <Page>
      <TitleBar
        title="Sample App"
        primaryAction={{
          content: 'Select products',
          onAction: () => setOpen(true),
        }}
      />
      <ResourcePicker
        resourceType="Product"
        showVariants={false}
        open={open}
        onSelection={handleSelection}
        onCancel={() => setOpen(false)}
      />
      <Layout>
        {emptyState && (
          <EmptyState
            heading="Discount your products temporarily"
            action={{
              content: 'Select products',
              onAction: () => setOpen(true),
            }}
            image={img}
          >
            <p>Select products to change their price temporarily.</p>
          </EmptyState>
        )}
      </Layout>
      {!emptyState && (
        <>
          <br />
          <ResourceListWithProducts />
        </>
      )}
    </Page>
  );
};

Index.propTypes = {};

export default Index;
